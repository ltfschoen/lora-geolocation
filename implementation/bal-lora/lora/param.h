#ifndef __PARAM_H__
#define __PARAM_H__

#define PARAM_DEV_EUI		0
#define PARAM_DEV_ID		1
#define PARAM_MODE			2
#define PARAM_TEST			3
#define PARAM_PACKET_NO	4
#define PARAM_COUNT			5

#define PARAM_MAX_LEN	6	/* sizeof(deveui) */

void	param_init(void);
int	param_get(int idx, uint8_t *data, uint8_t len);
int	param_set(int idx, uint8_t *data, uint8_t len);

#endif /* __PARAM_H__ */
