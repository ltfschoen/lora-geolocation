#ifndef __LORA_H__
#define __LORA_H__

/*
 * Notification bits reservation
 * bit #0 is always assigned to BLE event queue notification
 */
#define EV_NOTIF_LORA_DIO 	(1 << 5)
#define EV_NOTIF_BTN_PRESS 	(1 << 6)
#define EV_NOTIF_CONS_RX 		(1 << 7)
#define EV_NOTIF_GPS_RX 		(1 << 8)
#define EV_NOTIF_LORA_TX 		(1 << 9)
#define EV_NOTIF_LORA_RX 		(1 << 10)
#define EV_NOTIF_LORA_ACK 	(1 << 11)

#define MAX_CHANNELS				8

typedef enum
{
	BAL_CMD_NONE 		= 0x00,
	BAL_CMD_LORA_RESET,
	BAL_CMD_LORA_RX,
	BAL_CMD_LORA_TX,
	BAL_CMD_LORA_RX_7_7_125,
	BAL_CMD_LORA_RX_7_12_125,
	BAL_CMD_LORA_RX_7_7_500,
	BAL_CMD_LORA_RX_7_12_500,
	BAL_CMD_LORA_ACK
}bal_cmd_t;

void    lora_task_func(void *);
void		lora_receive(uint8_t chan, uint8_t	sf, \
	uint8_t	bw, bool inverted);
void		lora_send(uint8_t chan, uint8_t	sf, \
	uint8_t	bw, int8_t txpow, uint16_t interval, \
	uint16_t n, uint8_t cmd);
void		lora_stop(void);
void		lora_init(void* irq);
uint32_t lora_get_timestamp(void);
void		lora_task_notify_event(uint32_t ev);

#endif /* __LORA_H__ */
