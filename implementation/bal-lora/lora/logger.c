/*
 * logger.c
 *
 *  Created on: 7 Jul 2019
 *      Author: balog
 */
#include <stdint.h>

#include <hw_gpio.h>
#include <hw_uart.h>

#include "osal.h"
#include <ad_nvparam.h>
#include "hw/hw.h"
#include "hw/i2c.h"
#include "lora/logger.h"
#include "lora/param.h"
#include "ubxproto/ubx.h"

//#define DEBUG
#define LOG_TO_FLASH

#define LOGGER_DATA_MAX		1024
#define FLASH_DATA_MAX		0x00020000
#define UBX_M8_FIFOSIZE		128

#define UBX_M8_DDC_BYTES_AV_HI	0xFD
#define UBX_M8_DDC_BYTES_AV_LO	0xFE
#define UBX_M8_DDC_DATA_STREAM	0xFF

PRIVILEGED_DATA static logger_data_t data_to_log[LOGGER_DATA_MAX];
PRIVILEGED_DATA static uint16_t logged_count = 0;

PRIVILEGED_DATA static uint8_t			ubx_m8_fifo[UBX_M8_FIFOSIZE];
PRIVILEGED_DATA static uint16_t			ubx_m8_av_bytes = 0;
PRIVILEGED_DATA static UBXMsgBuffer ubx_buf;
//PRIVILEGED_DATA static UBXMsgBuffer ack_buf;

static const uart_config	uart_cfg = {
	.baud_rate	= HW_UART_BAUDRATE_9600,
	.data				= HW_UART_DATABITS_8,
	.parity			= HW_UART_PARITY_NONE,
	.stop				= HW_UART_STOPBITS_1,
	.auto_flow_control	= 0,
	.use_dma		= 0,
	.use_fifo		= 1,
};

static void uart_isr_ubx()
{
	switch (hw_uart_get_interrupt_id(HW_UART2)) {
		case HW_UART_INT_RECEIVED_AVAILABLE:
			if (!hw_uart_is_data_ready(HW_UART2))
				break;
			while (!hw_uart_read_buf_empty(HW_UART2)) {
				uint8_t	c;

				if (ubx_m8_av_bytes >= UBX_M8_FIFOSIZE) {
					ubx_m8_av_bytes = 0;
					continue;
				}
				c = hw_uart_read(HW_UART2);
				ubx_m8_fifo[ubx_m8_av_bytes++] = c;
				if ((ubx_m8_fifo[0] == '$') && (c == '\n')) {
#ifdef DEBUG
					char debug_ubx[] = "ubx rx: ";
					hw_uart_write_buffer(HW_UART1, debug_ubx, sizeof(debug_ubx));
					hw_uart_write_buffer(HW_UART1, ubx_m8_fifo, ubx_m8_av_bytes);
#endif
					ubx_m8_av_bytes = 0;
				}
			}
		break;
		default:
		break;
	}
}

void logger_init(void)
{
	hw_gpio_set_pin_function(HW_UBX_UART_TX_PORT,
		HW_UBX_UART_TX_PIN, HW_GPIO_MODE_OUTPUT, HW_GPIO_FUNC_UART2_TX);
	hw_gpio_set_pin_function(HW_UBX_UART_RX_PORT,
		HW_UBX_UART_RX_PIN, HW_GPIO_MODE_INPUT, HW_GPIO_FUNC_UART2_RX);

	hw_uart_init(HW_UART2, &uart_cfg);
	hw_uart_set_isr(HW_UART2, uart_isr_ubx);

	NVIC_DisableIRQ(UART2_IRQn);
	HW_UART_REG_SETF(HW_UART2, IER_DLH, ERBFI_dlh0, true);
	NVIC_EnableIRQ(UART2_IRQn);

	OS_DELAY_MS(5);

	ubx_buf = getCFG_MSG_RATE(UBXMsgClassNMEA, UBXMsgIdNMEA_GGA, 0);
	hw_uart_write_buffer(HW_UART2, (uint8_t *)ubx_buf.data, ubx_buf.size);
	clearUBXMsgBuffer(&ubx_buf);

	ubx_buf = getCFG_MSG_RATE(UBXMsgClassNMEA, UBXMsgIdNMEA_GLL, 0);
	hw_uart_write_buffer(HW_UART2, (uint8_t *)ubx_buf.data, ubx_buf.size);
	clearUBXMsgBuffer(&ubx_buf);

	ubx_buf = getCFG_MSG_RATE(UBXMsgClassNMEA, UBXMsgIdNMEA_GSA, 0);
	hw_uart_write_buffer(HW_UART2, (uint8_t *)ubx_buf.data, ubx_buf.size);
	clearUBXMsgBuffer(&ubx_buf);

	ubx_buf = getCFG_MSG_RATE(UBXMsgClassNMEA, UBXMsgIdNMEA_GSV, 0);
	hw_uart_write_buffer(HW_UART2, (uint8_t *)ubx_buf.data, ubx_buf.size);
	clearUBXMsgBuffer(&ubx_buf);

	ubx_buf = getCFG_MSG_RATE(UBXMsgClassNMEA, UBXMsgIdNMEA_RMC, 0);
	hw_uart_write_buffer(HW_UART2, (uint8_t *)ubx_buf.data, ubx_buf.size);
	clearUBXMsgBuffer(&ubx_buf);

	ubx_buf = getCFG_MSG_RATE(UBXMsgClassNMEA, UBXMsgIdNMEA_VTG, 0);
	hw_uart_write_buffer(HW_UART2, (uint8_t *)ubx_buf.data, ubx_buf.size);
	clearUBXMsgBuffer(&ubx_buf);

	ubx_buf = getCFG_MSG_RATE(UBXMsgClassNMEA, UBXMsgIdNMEA_ZDA, 0);
	hw_uart_write_buffer(HW_UART2, (uint8_t *)ubx_buf.data, ubx_buf.size);
	clearUBXMsgBuffer(&ubx_buf);

	ubx_buf = getCFG_MSG_RATE(UBXMsgClassTIM, UBXMsgIdTIM_TM2, 1);
	hw_uart_write_buffer(HW_UART2, (uint8_t *)ubx_buf.data, ubx_buf.size);
	clearUBXMsgBuffer(&ubx_buf);

#ifdef LOG_TO_FLASH
	param_get(PARAM_COUNT, (uint8_t *)&logged_count, sizeof(logged_count));
#endif
}

void logger_add(logger_data_t *data)
{
	if(data){
		UBXMsg last_msg;
		UBXTIM_TM2 timestamp;
		initMsg(&last_msg, sizeof(timestamp), UBXMsgClassTIM, UBXMsgIdTIM_TM2);
		if(memcmp(ubx_m8_fifo, &last_msg, 6) == 0){
			last_msg.payload.TIM_TM2 = timestamp;
			memcpy(&timestamp, &ubx_m8_fifo[6], sizeof(timestamp));
			data->timemark_ms = timestamp.towMsR;
			data->timemark_ns = timestamp.towSubMsR;
			memcpy(&data_to_log[(data->counter % LOGGER_DATA_MAX)], data, sizeof(logger_data_t));
			logger_save(data->counter);
		}else{
			// Timemark wasn't received correctly or this device is not connected to a UBX module.
			printf("ERROR at %d %d\r\n", logged_count, data->counter);
			// Save the data anyway for RSSI.
			logger_save(data->counter);
			memset(data, 0x00, sizeof(logger_data_t));
		}
		memset(ubx_m8_fifo, 0x00, UBX_M8_FIFOSIZE);
		ubx_m8_av_bytes = 0;
	}
}

void logger_save(uint16_t addr)
{
#ifdef LOG_TO_FLASH
	nvms_t	nvms;
	if((addr + 1) * sizeof(logger_data_t) < FLASH_DATA_MAX){
		nvms = ad_nvms_open(NVMS_LOG_PART);
		ad_nvms_write(nvms, (addr * sizeof(logger_data_t)), (uint8_t *)&data_to_log[addr % LOGGER_DATA_MAX], sizeof(logger_data_t));
	}else{
		nvms = ad_nvms_open(NVMS_GENERIC_PART);
		ad_nvms_write(nvms, (32 + (addr % 6553) * sizeof(logger_data_t)), (uint8_t *)&data_to_log[addr % LOGGER_DATA_MAX], sizeof(logger_data_t));
	}
	logged_count++;
	param_set(PARAM_COUNT, (uint8_t *)&logged_count, sizeof(logged_count));
#endif
}

void logger_print(uint16_t start, uint16_t end)
{
	uint16_t size = (end - start);
	OS_ASSERT(size <= LOGGER_DATA_MAX);
#ifdef LOG_TO_FLASH
	nvms_t	nvms;
	if((start + 1) * sizeof(logger_data_t) < FLASH_DATA_MAX){
		nvms = ad_nvms_open(NVMS_LOG_PART);
		ad_nvms_read(nvms, (start * sizeof(logger_data_t)), (uint8_t *)data_to_log, (size * sizeof(logger_data_t)));
	}else{
		start = start % 6553; //(FLASH_DATA_MAX / sizeof(logger_data_t));
		nvms = ad_nvms_open(NVMS_GENERIC_PART);
		ad_nvms_read(nvms, (32 + start * sizeof(logger_data_t)), (uint8_t *)data_to_log, (size * sizeof(logger_data_t)));
	}
	param_get(PARAM_COUNT, (uint8_t *)&logged_count, sizeof(logged_count));
#endif
	printf("%d\r\n", logged_count);
	for (uint16_t i = 0; i < size ; i++) {
		printf("%d,%d,%d.%02d,%ld,%ld,%ld,%ld\r\n", \
			data_to_log[i].counter, data_to_log[i].rssi - 256, \
			(data_to_log[i].snr >> 2), ((data_to_log[i].snr & 0x03) * 25), \
			data_to_log[i].timemark_ms, data_to_log[i].timemark_ns, \
			data_to_log[i].lat, data_to_log[i].lon);
	}
	logged_count = 0;
#ifdef LOG_TO_FLASH
	param_set(PARAM_COUNT, (uint8_t *)&logged_count, sizeof(logged_count));
#endif
}
