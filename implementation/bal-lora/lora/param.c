/* Parameter handling */

#include <stdint.h>
#include "osal.h"

#include <ad_nvparam.h>
#include <platform_nvparam.h>
#include "lora/param.h"
#include "lora/util.h"

#define DEBUG

/* EUI-48: 78af58040000  EUI-64: 78af58fffe040000 */
INITIALISED_PRIVILEGED_DATA static uint8_t	deveui[6] = {
	0x00, 0x00, 0x04, 0x58, 0xaf, 0x78,
};

PRIVILEGED_DATA static uint8_t	dev_id, mode, test, packet_no;

PRIVILEGED_DATA static uint16_t count;

/* NVPARAM "ble_platform" */
#define PARAM_DEV_EUI_OFF	TAG_BLE_PLATFORM_BD_ADDRESS
#define PARAM_DEV_EUI_LEN	sizeof(deveui)

/* VES */
#define PARAM_DEV_ID_OFF	0
#define PARAM_DEV_ID_LEN	sizeof(dev_id)

#define PARAM_MODE_OFF	(PARAM_DEV_ID_OFF + PARAM_DEV_ID_LEN)
#define PARAM_MODE_LEN	sizeof(mode)

#define PARAM_TEST_OFF	(PARAM_MODE_OFF + PARAM_MODE_LEN)
#define PARAM_TEST_LEN	sizeof(test)

#define PARAM_PACKET_NO_OFF (PARAM_TEST_OFF + PARAM_TEST_LEN)
#define PARAM_PACKET_NO_LEN	sizeof(packet_no)

#define PARAM_COUNT_OFF	(PARAM_PACKET_NO_OFF + PARAM_PACKET_NO_LEN)
#define PARAM_COUNT_LEN	sizeof(count)

#define PARAM_FLAG_BLE_NV	0x01	/* Stored in BLE NVPARAM area */
#define PARAM_FLAG_REVERSE	0x02	/* Reversed in protocol */
#define PARAM_FLAG_WRITE_ONLY	0x04	/* "Get param" disallowed */

struct param_def {
	void		*mem;		/* Location in memory */
	uint16_t	 offset;	/* Location in permanent storage */
	uint8_t		 len;		/* Length */
	uint8_t		 flags;		/* Flags */
};

static const char param_txt[PARAM_MAX_LEN][PARAM_MAX_LEN] = {
	"deveu",
	"devid",
	"mode ",
	"test ",
	"txno ",
	"count"
};

static const struct param_def	params[] = {
	[PARAM_DEV_EUI] = {
		.mem	= deveui,
		.offset	= PARAM_DEV_EUI_OFF,
		.len	= PARAM_DEV_EUI_LEN,
		.flags	= PARAM_FLAG_BLE_NV | PARAM_FLAG_REVERSE,
	},
	[PARAM_DEV_ID] = {
		.mem	= &dev_id,
		.offset	= PARAM_DEV_ID_OFF,
		.len	= PARAM_DEV_ID_LEN,
	},
	[PARAM_MODE] = {
		.mem	= &mode,
		.offset	= PARAM_MODE_OFF,
		.len	= PARAM_MODE_LEN,
	},
	[PARAM_TEST] = {
		.mem	= &test,
		.offset	= PARAM_TEST_OFF,
		.len	= PARAM_TEST_LEN,
	},
	[PARAM_PACKET_NO] = {
		.mem	= &packet_no,
		.offset	= PARAM_PACKET_NO_OFF,
		.len	= PARAM_PACKET_NO_LEN,
	},
	[PARAM_COUNT] = {
		.mem	= &count,
		.offset	= PARAM_COUNT_OFF,
		.len	= PARAM_COUNT_LEN,
	},
};

static inline void
reverse_memcpy(void *dest, void *src, size_t len)
{
	char	*d = dest, *s = src;

	while (len) {
		d[--len] = *s++;
	}
}

/* Read param from permanent storage into memory */
static void
read_param(const struct param_def *param)
{
	uint8_t	buf[PARAM_MAX_LEN];

	if (param->flags & PARAM_FLAG_BLE_NV) {
		nvparam_t	nvparam;
		uint16_t	param_len;
		uint8_t		valid;

		nvparam = ad_nvparam_open("ble_platform");
		param_len = ad_nvparam_get_length(nvparam, param->offset, NULL);
		OS_ASSERT(param_len == param->len + 1);
		ad_nvparam_read_offset(nvparam, param->offset,
		    param_len - sizeof(valid), sizeof(valid), &valid);
		if (valid != 0x00)
			return;
		ad_nvparam_read(nvparam, param->offset, param->len, param->mem);
	} else {
		nvms_t	nvms;
		int	i;

		OS_ASSERT(sizeof(buf) >= param->len);
		nvms = ad_nvms_open(NVMS_GENERIC_PART);
		ad_nvms_read(nvms, param->offset, buf, param->len);
		for (i = 0; i < param->len; i++) {
			if (buf[i] != 0xff) {
				memcpy(param->mem, buf, param->len);
				return;
			}
		}
	}
}

/* Set param in memory and write it to permanent storage */
static void
write_param(const struct param_def *param, void *data)
{
	uint8_t		buf[PARAM_MAX_LEN + 1];

	OS_ASSERT(param->len <= sizeof(buf));
	if (param->flags & PARAM_FLAG_REVERSE)
		reverse_memcpy(buf, data, param->len);
	else
		memcpy(buf, data, param->len);
	memcpy(param->mem, buf, param->len);
	if (param->flags & PARAM_FLAG_BLE_NV) {
		nvparam_t	nvparam;
		uint16_t	param_len;

		nvparam = ad_nvparam_open("ble_platform");
		param_len = ad_nvparam_get_length(nvparam, param->offset, NULL);
		OS_ASSERT(param_len == param->len + 1);
		OS_ASSERT(param_len <= sizeof(buf));
		(void)param_len;
		buf[param->len] = 0x00;
		ad_nvparam_write(nvparam, param->offset, param->len + 1, buf);
		//ad_nvms_flush(ad_nvms_open(NVMS_PARAM_PART), 0);
	} else {
		nvms_t		nvms;

		nvms = ad_nvms_open(NVMS_GENERIC_PART);
		ad_nvms_write(nvms, param->offset, buf, param->len);
	}
}

int
param_get(int idx, uint8_t *data, uint8_t len)
{
	if (idx >= (int)ARRAY_SIZE(params) || params[idx].len > len ||
	    (params[idx].flags & PARAM_FLAG_WRITE_ONLY))
		return 0;
	if (!(params[idx].flags & PARAM_FLAG_WRITE_ONLY)) {
		if (params[idx].flags & PARAM_FLAG_REVERSE)
			reverse_memcpy(data, params[idx].mem, params[idx].len);
		else
			memcpy(data, params[idx].mem, params[idx].len);
	}
	return params[idx].len;
}

int
param_set(int idx, uint8_t *data, uint8_t len)
{
	if (idx >= (int)ARRAY_SIZE(params) || params[idx].len != len)
		return -1;
	write_param(params + idx, data);
	return 0;
}

void
param_init(void)
{
	int	i;

	for (i = 0; i < (int)ARRAY_SIZE(params); i++) {
		read_param(params + i);
#ifdef DEBUG
		printf("%s:x",&param_txt[i][0]);
		if (params[i].flags & PARAM_FLAG_REVERSE) {
			for (int j = params[i].len - 1; j >= 0; j--)
				printf("%02x", ((uint8_t *)params[i].mem)[j]);
		} else {
			for (int j = 0; j < params[i].len; j++)
				printf("%02x", ((uint8_t *)params[i].mem)[j]);
		}
		printf("\r\n");
#endif
	}
}
