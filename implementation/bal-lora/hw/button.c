#include <stdio.h>

#include <sys_rtc.h>

#include "lora/lora.h"
#include "hw/button.h"
#include "hw/hw.h"
#include "hw/cons.h"
#include "osal.h"

#ifdef FEATURE_USER_BUTTON

#ifdef FEATURE_USER_BUTTON_PRINT

void
button_press(uint32_t time)
{
	(void)time;
	printf("BUTTON\r\n");
}

#else /* !FEATURE_USER_BUTTON_PRINT */

#define LONG_PRESS_TIME	sec2osticks(10)

#if HW_USER_BTN_ACTIVE == HW_WKUP_PIN_STATE_HIGH
#define USER_BTN_DOWN(port, pin)	hw_gpio_get_pin_status(port, pin)
#else
#define USER_BTN_DOWN(port, pin)	!hw_gpio_get_pin_status(port, pin)
#endif

PRIVILEGED_DATA static ostime_t	press_time;

static void
button_cb(osjob_t *job)
{
	ostime_t	now = os_getTime();

	if (now - press_time >= LONG_PRESS_TIME)
		upgrade_reboot(UPGRADE_DEFAULT);
	else if (!USER_BTN_DOWN(HW_USER_BTN_PORT, HW_USER_BTN_PIN))
		lora_send();
	else
		os_setTimedCallback(job, now + ms2osticks(20), button_cb);
}

void
button_press(ostime_t t)
{
	PRIVILEGED_DATA static osjob_t	button_job;

	press_time = t;
	button_cb(&button_job);
}

#endif /* FEATURE_USER_BUTTON_PRINT */

#endif /* FEATURE_USER_BUTTON */
