#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <hw_gpio.h>
#include <hw_uart.h>
#include <resmgmt.h>
#include <sys_rtc.h>

#include "hw/hw.h"
#include "hw/iox.h"
#include "hw/led.h"
#include "hw/cons.h"
#include "lora/lora.h"
#include "lora/logger.h"
#include "lora/param.h"
#include "lora/util.h"
#include "sensor/gps.h"

#include "radio.h"
#include "sx1276/sx1276.h"

#define CONSOLE_INPUT

#ifdef HW_FWD_UART_TX_PORT
#define CONSOLE_FWD
#endif

#define CONSOLE_FWD_FD	3

#ifdef CONFIG_CUSTOM_PRINT

int
_write(int fd, const void *ptr, int len)
{
	switch (fd) {
#ifdef CONSOLE_FWD
	case CONSOLE_FWD_FD:
		hw_uart_write_buffer(HW_UART2, ptr, len);
		break;
#endif
	default:
		hw_uart_write_buffer(HW_UART1, ptr, len);
		break;
	}
	return len;
}

#ifdef CONSOLE_INPUT

#define isasciispace(c)	((uint8_t)(c) <= ' ')

#define CTRL(x)	((x) ^ 0x40)
#define ESC	CTRL('[')

static const char	BELL[]		= { '\a' };
static const char	BACKSPACE[]	= { '\b', ' ', '\b' };
static const char	CRLF[]		= { '\r', '\n' };
static const char	CTRL_C_CRLF[]	= { '^', 'C', '\r', '\n' };
static const char	CTRL_R_CRLF[]	= { '^', 'R', '\r', '\n' };
static const char	CR_ERASE_LINE[]	= { '\r', ESC, '[', '2', 'K' };
#ifdef CONSOLE_FWD
static const char	CTRL_C[]	= { CTRL('C') };
#endif
static const char	hex[] = {
	'0', '1', '2', '3', '4', '5', '6', '7',
	'8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
};

static PRIVILEGED_DATA char	cons_line[128];	/* input line */
static PRIVILEGED_DATA uint8_t	cons_len;	/* line length */

#ifdef CONSOLE_FWD
static PRIVILEGED_DATA char	fwd_line[128];	/* input line */
static PRIVILEGED_DATA uint8_t	fwd_len;	/* line length */
#endif

/* Circular input buffer.  Size MUST be a power of two, <= 128 */
static PRIVILEGED_DATA char	cons_cbuf[16];
#ifdef CONSOLE_FWD
static PRIVILEGED_DATA char	fwd_cbuf[128];
#endif
/* Read and write indices into cons_cbuf[].  The write index is written only
 * from uart_isr(); the read index only from cons_rx().  The indices are used
 * for accessing cons_cbuf modulo sizeof(cons_cbuf); thus allowing to
 * distinguish between empty and full buffer. */
static PRIVILEGED_DATA volatile uint8_t	cons_ridx, cons_widx;
static PRIVILEGED_DATA volatile uint8_t	cons_pending;	/* input pending */
#define CBUF_IDX(i)	((i) & (sizeof(cons_cbuf) - 1))
#ifdef CONSOLE_FWD
static PRIVILEGED_DATA volatile uint8_t	fwd_ridx, fwd_widx;
static PRIVILEGED_DATA volatile uint8_t	fwd_pending;	/* input pending */
#define FWD_CBUF_IDX(i)	((i) & (sizeof(fwd_cbuf) - 1))
#endif

static inline void
backspace()
{
	_write(1, BACKSPACE, sizeof(BACKSPACE));
	cons_len--;
}

static void
cmd_help(int argc, char **argv)
{
	(void)argc;
	(void)argv;
	printf("commands:\r\n"
	    "help\r\n"
	    "\tShow this help\r\n"
	    "gpio port pin [0|1|i]\r\n"
	    "\tGet or configure and set a GPIO pin\r\n"
	    "iox pin [0|1|i]\r\n"
	    "\tGet or configure and set one I/O expander pin\r\n"
	    "ioxconf [conf]\r\n"
	    "\tConfigure all I/O expander pins\r\n"
	    "ioxpins [pins]\r\n"
	    "\tGet or set all I/O expander pins\r\n"
	    "led [test|rebooting]\r\n"
	    "\tBlink LED\r\n"
	    "param num [data]\r\n"
	    "\tGet or set NVPARAM/NVMS variable\r\n"
	    "reset\r\n"
	    "\tReset board\r\n"
	    "rx chan [sf [bw]]\r\n"
	    "\tReceive continuously\r\n"
	    "\tSee \"tx\" for details\r\n"
	    "rxnoinv chan [sf [bw]]\r\n"
	    "\tReceive continuously without I/Q inversion\r\n"
	    "\tSee \"tx\" for details\r\n"
	    "sense\r\n"
	    "\tGet sensor data\r\n"
	    "sensprep\r\n"
	    "\tPrepare sensor data\r\n"
	    "stop\r\n"
	    "\tStop TX/RX\r\n"
	    "tx chan [sf [bw [pow [iv [n]]]]]\r\n"
	    "\tTransmit continuously\r\n"
	    "\tchan:\tChannel (0-7)\r\n");
	printf(
	    "\tsf:\tSpread factor (7-12, default 7)\r\n"
	    "\tbw:\tBandwidth (125, 250 or 500, default 125)\r\n"
	    "\tpow:\tTX power (1-20, default channel-specific)\r\n"
	    "\tiv:\tInterval in ms (0-65535, default 0)\r\n"
	    "\tn:\tNumber of packets to send (0-65535, 0 = infinite, default 0)\r\n");
}

static void
cmd_gpio(int argc, char **argv)
{
	const char	*errstr;
	uint8_t		 port, pin;

	port = strtonum(argv[1], 0, 4, &errstr);
	if (errstr) {
		printf("%s: %s\r\n", argv[1], errstr);
		return;
	}
	pin = strtonum(argv[2], 0, 7, &errstr);
	if (errstr) {
		printf("%s: %s\r\n", argv[2], errstr);
		return;
	}
	if (argc > 3) {
		if (strlen(argv[3]) != 1) {
			printf("%s: should be 0, 1 or i\r\n", argv[3]);
			return;
		}
		switch (argv[3][0]) {
		case '0':
		case '1':
			hw_gpio_configure_pin(port, pin, HW_GPIO_MODE_OUTPUT,
			    HW_GPIO_FUNC_GPIO, argv[3][0] - '0');
			break;
		case 'i':
			hw_gpio_set_pin_function(port, pin, HW_GPIO_MODE_INPUT,
			    HW_GPIO_FUNC_GPIO);
			break;
		default:
			printf("%s: should be 0, 1 or i\r\n", argv[3]);
			return;
		}
	} else {
		printf("%d\r\n", hw_gpio_get_pin_status(port, pin));
	}
}

static void
cmd_iox(int argc, char **argv)
{
	const char	*errstr;
	int		 val;
	uint8_t		 pin;

	pin = strtonum(argv[1], 0, 15, &errstr);
	if (errstr) {
		printf("%s: %s\r\n", argv[1], errstr);
		return;
	}
	if (argc > 2) {
		if (strlen(argv[2]) != 1) {
			printf("%s: should be 0, 1 or i\r\n", argv[3]);
			return;
		}
		switch (argv[2][0]) {
		case '0':
		case '1':
			if (iox_conf(pin, false) == -1) {
				printf("failed\r\n");
				return;
			}
			if (iox_set(pin, argv[2][0] - '0') == -1) {
				printf("failed\r\n");
				return;
			}
			break;
		case 'i':
			if (iox_conf(pin, true) == -1) {
				printf("failed\r\n");
				return;
			}
			break;
		default:
			printf("%s: should be 0, 1 or i\r\n", argv[3]);
			return;
		}
	} else {
		if ((val = iox_get(pin)) == -1) {
			printf("failed\r\n");
			return;
		}
		printf("%d\r\n", val);
	}
}

static void
cmd_ioxconf(int argc, char **argv)
{
	const char	*hi, *lo;
	size_t		 i;
	int		 conf;
	uint8_t		 buf[2];

	if (argc > 1) {
		if (strlen(argv[1]) != ARRAY_SIZE(buf) * 2) {
			printf("%s: invalid length\r\n", argv[2]);
			return;
		}
		for (i = 0; i < ARRAY_SIZE(buf); i++) {
			if ((hi = memchr(hex, argv[1][i<<1], sizeof(hex)))
			    == NULL) {
				printf("%c: invalid character\r\n",
				    argv[1][i<<1]);
				return;
			}
			if ((lo = memchr(hex, argv[1][(i<<1) + 1], sizeof(hex)))
			    == NULL) {
				printf("%c: invalid character\r\n",
				    argv[1][(i<<1) + 1]);
				return;
			}
			buf[i] = (hi - hex) << 4 | (lo - hex);
		}
		conf = (int)buf[0] << 8 | buf[1];
		if (iox_setconf(conf) == -1) {
			printf("failed\r\n");
			return;
		}
	} else {
		if ((conf = iox_getconf()) == -1) {
			printf("failed\r\n");
			return;
		}
		buf[0] = conf >> 8;
		buf[1] = conf;
		for (i = 0; i < sizeof(buf); i++)
			printf("%02x", buf[i]);
		printf("\r\n");
	}
}

static void
cmd_ioxpins(int argc, char **argv)
{
	const char	*hi, *lo;
	size_t		 i;
	int		 pins;
	uint8_t		 buf[2];

	if (argc > 1) {
		if (strlen(argv[1]) != ARRAY_SIZE(buf) * 2) {
			printf("%s: invalid length\r\n", argv[2]);
			return;
		}
		for (i = 0; i < ARRAY_SIZE(buf); i++) {
			if ((hi = memchr(hex, argv[1][i<<1], sizeof(hex)))
			    == NULL) {
				printf("%c: invalid character\r\n",
				    argv[1][i<<1]);
				return;
			}
			if ((lo = memchr(hex, argv[1][(i<<1) + 1], sizeof(hex)))
			    == NULL) {
				printf("%c: invalid character\r\n",
				    argv[1][(i<<1) + 1]);
				return;
			}
			buf[i] = (hi - hex) << 4 | (lo - hex);
		}
		pins = (int)buf[0] << 8 | buf[1];
		if (iox_setpins(pins) == -1) {
			printf("failed\r\n");
			return;
		}
	} else {
		if ((pins = iox_getpins()) == -1) {
			printf("failed\r\n");
			return;
		}
		buf[0] = pins >> 8;
		buf[1] = pins;
		for (i = 0; i < sizeof(buf); i++)
			printf("%02x", buf[i]);
		printf("\r\n");
	}
}

static void
cmd_led(int argc, char **argv)
{
	uint8_t	state = LED_STATE_IDLE;

	if (argc > 1) {
		if (strlen(argv[1]) <= 4 &&
		    strncmp(argv[1], "test", strlen(argv[1])) == 0) {
			state = LED_STATE_TEST;
		} else if (strlen(argv[1]) <= 9 &&
		    strncmp(argv[1], "rebooting", strlen(argv[1])) == 0) {
			state = LED_STATE_REBOOTING;
		} else {
			printf("%s: invalid colour\r\n", argv[1]);
			return;
		}
	}
	led_notify(state);
}

static void
cmd_param(int argc, char **argv)
{
	uint8_t		 buf[PARAM_MAX_LEN];
	const char	*errstr, *hi, *lo;
	uint8_t		 idx, len, i;

	idx = strtonum(argv[1], 0, 255, &errstr);
	if (errstr) {
		printf("%s: %s\r\n", argv[1], errstr);
		return;
	}
	len = param_get(idx, buf, sizeof(buf));
	if (argc > 2) {
		if (len != 0) {
			if (strlen(argv[2]) != len * 2) {
				printf("%s: invalid length\r\n", argv[2]);
				return;
			}
		} else {
			if ((strlen(argv[2]) & 1)) {
				printf("%s: invalid length\r\n", argv[2]);
				return;
			}
			len = strlen(argv[2]) / 2;
		}
		for (i = 0; i < len; i++) {
			if ((hi = memchr(hex, argv[2][i<<1], sizeof(hex)))
			    == NULL) {
				printf("%c: invalid character\r\n",
				    argv[2][i<<1]);
				return;
			}
			if ((lo = memchr(hex, argv[2][(i<<1) + 1], sizeof(hex)))
			    == NULL) {
				printf("%c: invalid character\r\n",
				    argv[2][(i<<1) + 1]);
				return;
			}
			buf[i] = (hi - hex) << 4 | (lo - hex);
		}
		if (param_set(idx, buf, len) == -1) {
			printf("cannot set param\r\n");
			return;
		}
	} else {
		if (len == 0) {
			printf("param not found\r\n");
			return;
		}
		for (i = 0; i < len; i++)
			printf("%02x", buf[i]);
		printf("\r\n");
	}
}

static void
cmd_sense(int argc, char **argv)
{
	char	buf[32];
	size_t j, len;

	(void)argc;
	(void)argv;
	gps_print_sats();
	if ((len = gps_read(buf, sizeof(buf))) != 0) {
		for (j = 0; j < len; j++)
			printf("%02x", (uint8_t)buf[j]);
		printf("\r\n");
	}
}

#define MAX_SENSOR_SAMPLE_TIME	portCONVERT_MS_2_TICKS(2000)
PRIVILEGED_DATA static OS_TIMER	sensor_timer;
PRIVILEGED_DATA static uint32_t	sampling_since;

static void
sense_wait(OS_TIMER timer)
{
	uint32_t	now, delay;

	now = rtc_get();
	if (now < sampling_since + MAX_SENSOR_SAMPLE_TIME &&
	    (delay = gps_data_ready()) != 0) {
		OS_TIMER_START(timer, OS_TIMER_FOREVER);
		return;
	}
	printf("sensor ready\r\n");
}

static void
cmd_sensprep(int argc, char **argv)
{
	(void)argc;
	(void)argv;
	sampling_since = rtc_get();
	gps_prepare();
	if(sensor_timer == NULL){
		/* create timer for the sensors */
		sensor_timer = OS_TIMER_CREATE("sensprep", OS_MS_2_TICKS(100),
			OS_TIMER_FAIL, (void *) OS_GET_CURRENT_TASK(), sense_wait);

		OS_ASSERT(sensor_timer);
	}
	OS_TIMER_START(sensor_timer, OS_TIMER_FOREVER);
}

static void
cmd_stop(int argc, char **argv)
{
	(void)argc;
	(void)argv;
	lora_stop();
}

static void
cmd_tx(int argc, char **argv)
{
	const char	*errstr;
	uint8_t		 chan;
	uint8_t		 sf = 7;
	uint8_t		 bw = 0;
	uint16_t	 n = 0, interval = 0;
	int8_t		 txpow = 0;
	uint8_t		 cmd = 0;

	chan = strtonum(argv[1], 0, 12, &errstr);
	if (errstr) {
		printf("%s: %s\r\n", argv[1], errstr);
		return;
	}
	if (argc > 2) {
		sf = strtonum(argv[2], 7, 12, &errstr);
		if (errstr) {
			printf("%s: %s\r\n", argv[2], errstr);
			return;
		}
	}
	if (argc > 3) {
		if (strcmp(argv[3], "125") == 0)
			bw = 0;
		else if (strcmp(argv[3], "250") == 0)
			bw = 1;
		else if (strcmp(argv[3], "500") == 0)
			bw = 2;
		else {
			printf("%s: should be 125, 250 or 500\r\n", argv[3]);
			return;
		}
	}
	if (argc > 4) {
		txpow = strtonum(argv[4], 0, 20, &errstr);
		if (errstr) {
			printf("%s: %s\r\n", argv[4], errstr);
			return;
		}
	}
	if (argc > 5) {
		interval = strtonum(argv[5], 0, 0xffff, &errstr);
		if (errstr) {
			printf("%s: %s\r\n", argv[5], errstr);
			return;
		}
	}
	if (argc > 6) {
		n = strtonum(argv[6], 0, 0xffff, &errstr);
		if (errstr) {
			printf("%s: %s\r\n", argv[6], errstr);
			return;
		}
	}
	if (argc > 7) {
		cmd = strtonum(argv[7], 0, BAL_CMD_LORA_ACK, &errstr);
		if (errstr) {
			printf("%s: %s\r\n", argv[7], errstr);
			return;
		}
	}
	lora_send(chan, sf, bw, txpow, interval, n, cmd);
}

static void
cmd_rx(int argc, char **argv)
{
	const char	*errstr;
	int		 chan;
	uint8_t	sf = 7;
	uint8_t	bw = 0;
	bool		inv = false;

	if (strlen(argv[0]) > 2) {
		inv = true;
	}
	chan = strtonum(argv[1], 0, MAX_CHANNELS, &errstr);
	if (errstr) {
		printf("%s: %s\r\n", argv[1], errstr);
		return;
	}
	if (argc > 2) {
		sf = strtonum(argv[2], 7, 12, &errstr);
		if (errstr) {
			printf("%s: %s\r\n", argv[2], errstr);
			return;
		}
	}
	if (argc > 3) {
		if (strcmp(argv[3], "125") == 0)
			bw = 0;
		else if (strcmp(argv[3], "250") == 0)
			bw = 1;
		else if (strcmp(argv[3], "500") == 0)
			bw = 2;
		else {
			printf("%s: should be 125, 250 or 500\r\n", argv[3]);
			return;
		}
	}
	lora_receive(chan, sf, bw, inv);
}

static void
cmd_reset(int argc, char **argv)
{
	(void)argv;
	(void)argc;
	printf("Rebooting...\r\n");
	hw_cpm_reboot_system();
}

static void
cmd_getlog(int argc, char **argv)
{
	(void)argv;
	(void)argc;
	const char *errstr;
	uint16_t start = 0;
	uint16_t end = 0;

	if (argc > 2) {
		start = strtonum(argv[1], 0, 0xFFFF, &errstr);
		if (errstr) {
			printf("%s: %s\r\n", argv[1], errstr);
			return;
		}
		end = strtonum(argv[2], start, 0xFFFF, &errstr);
		if (errstr) {
			printf("%s: %s\r\n", argv[2], errstr);
			return;
		}
	}
	logger_print(start, end);
}

struct command {
	const char	*cmd;
	const char	 minargs, maxargs;
	void		(*handler)(int, char **);
};

static const struct command	cmd[] = {
	{ "gpio", 3, 4, cmd_gpio },
	{ "help", 1, 1, cmd_help },
	{ "iox", 2, 3, cmd_iox },
	{ "ioxconf", 1, 2, cmd_ioxconf },
	{ "ioxpins", 1, 2, cmd_ioxpins },
	{ "led", 1, 2, cmd_led },
	{ "param", 2, 3, cmd_param },
	{ "reset", 1, 1, cmd_reset },
	{ "rx", 2, 4, cmd_rx },
	{ "rxinv", 2, 4, cmd_rx },
	{ "sense", 1, 1, cmd_sense },
	{ "sensprep", 1, 1, cmd_sensprep },
	{ "stop", 1, 1, cmd_stop },
	{ "tx", 2, 8, cmd_tx },
	{ "getlog", 3, 3, cmd_getlog },
};

static int
tokenise(char **tokv, int toklen)
{
	char	*s;
	int	 tokc;

	if (cons_len >= sizeof(cons_line))
		return -1;
	cons_line[cons_len] = '\0';
	tokc = 0;
	for (s = cons_line; *s != '\0' && isasciispace(*s); s++)
		;
	while (*s != '\0') {
		tokv[tokc++] = s;
		if (tokc == toklen)
			break;
		while (*s != '\0' && !isasciispace(*s))
			s++;
		while (*s != '\0' && isasciispace(*s))
			*s++ = '\0';
	}
	return tokc;
}

static void
runcmd(const struct command *c, int tokc, char **tokv) {
	if (tokc < c->minargs || tokc > c->maxargs) {
		printf("%s: needs between %d and %d args\r\n",
		    c->cmd, c->minargs - 1, c->maxargs - 1);
		return;
	}
	c->handler(tokc, tokv);
}

static void
handle_line()
{
	char	*tokv[8];
	int	 tokc, i, cand;

#ifdef CONSOLE_FWD
	if (cons_len && cons_line[0] == '>') {
		if (cons_len == 2 && cons_line[1] == 'C') {
			write(CONSOLE_FWD_FD, CTRL_C, sizeof(CTRL_C));
		} else {
			uint8_t	s = 1;

			while (s < cons_len && isasciispace(cons_line[s]))
				s++;
			cons_line[cons_len++] = '\r';
			write(CONSOLE_FWD_FD, cons_line + s, cons_len - s);
		}
		return;
	}
#endif
	tokc = tokenise(tokv, (int)ARRAY_SIZE(tokv));
	if (tokc <= 0)
		return;
	cand = -1;
	for (i = 0; i < (int)ARRAY_SIZE(cmd); i++) {
		if (strcmp(cmd[i].cmd, tokv[0]) == 0) {
			runcmd(cmd + i, tokc, tokv);
			return;
		}
		if (strncmp(cmd[i].cmd, tokv[0], strlen(tokv[0])) == 0) {
			switch (cand) {
			case -1:
				cand = i;
				break;
			default:
				cand = -2;
				break;
			}
		}
	}
	if (cand >= 0) {
		runcmd(cmd + cand, tokc, tokv);
		return;
	}
	printf("%s: command not found\r\n", tokv[0]);
}

static void
proc_char(uint8_t c)
{
	switch (c) {
	case CTRL('C'):
		_write(1, CTRL_C_CRLF, sizeof(CTRL_C_CRLF));
		cons_len = 0;
		break;
	case CTRL('H'):
	case CTRL('?'):
		if (cons_len)
			backspace();
		break;
	case CTRL('U'):
		_write(1, CR_ERASE_LINE, sizeof(CR_ERASE_LINE));
		cons_len = 0;
		break;
	case CTRL('W'):
		while (cons_len && isasciispace(cons_line[cons_len - 1]))
			backspace();
		while (cons_len && !isasciispace(cons_line[cons_len - 1]))
			backspace();
		break;
	case CTRL('R'):
		_write(1, CTRL_R_CRLF, sizeof(CTRL_R_CRLF));
		_write(1, cons_line, cons_len);
		break;
	case '\r':
	case '\n':
		_write(1, CRLF, sizeof(CRLF));
		handle_line();
		cons_len = 0;
		break;
	default:
		if (c < 0x20 || c >= 0x80)
			break;
		if (cons_len >= sizeof(cons_line) - 1) {
			_write(1, BELL, sizeof(BELL));
			break;
		}
		cons_line[cons_len++] = c;
		_write(1, &c, 1);
		break;
	}
}

void
cons_rx()
{
	static volatile bool	busy;
	uint8_t			r;

	if (busy)
		return;
	busy = true;
	while (cons_pending) {
		cons_pending = 0;
		r = cons_ridx;
		while (r != cons_widx) {
			proc_char(cons_cbuf[CBUF_IDX(r++)]);
			cons_ridx = r;
		}
	}
	busy = false;
}

static void
uart_isr()
{
	uint8_t		c, w;

	switch (hw_uart_get_interrupt_id(HW_UART1)) {
	case HW_UART_INT_RECEIVED_AVAILABLE:
		if (!hw_uart_is_data_ready(HW_UART1))
			break;
		while (hw_uart_is_data_ready(HW_UART1)) {
			c = hw_uart_rxdata_getf(HW_UART1);
			w = cons_widx;
			if ((uint8_t)(w - cons_ridx) < sizeof(cons_cbuf)) {
				cons_cbuf[CBUF_IDX(w)] = c;
				BARRIER();
				cons_widx = w + 1;
			}
		}
		if (!cons_pending) {
			cons_pending = 1;
			lora_task_notify_event(EV_NOTIF_CONS_RX);
		}
		break;
	default:
		break;
	}
}

void
cons_fwd_rx()
{
#ifdef CONSOLE_FWD
	uint8_t			r, c;

	while (fwd_pending) {
		fwd_pending = 0;
		r = fwd_ridx;
		while (r != fwd_widx) {
			c = fwd_cbuf[FWD_CBUF_IDX(r++)];
			switch (c) {
			case '\n':
				fwd_line[fwd_len++] = c;
				write(1, "< ", 2);
				write(1, fwd_line, fwd_len);
				fwd_len = 0;
				break;
			default:
				if (fwd_len < sizeof(fwd_line) - 1)
					fwd_line[fwd_len++] = c;
				break;
			}
			fwd_ridx = r;
		}
	}
	ad_lora_suspend_sleep(LORA_SUSPEND_CONSOLE, sec2osticks(2));
#endif
}

#ifdef CONSOLE_FWD
static void
uart2_isr()
{
	uint8_t		c, w;

	switch (hw_uart_get_interrupt_id(HW_UART2)) {
	case HW_UART_INT_RECEIVED_AVAILABLE:
		if (!hw_uart_is_data_ready(HW_UART2))
			break;
		while (hw_uart_is_data_ready(HW_UART2)) {
			c = hw_uart_rxdata_getf(HW_UART2);
			w = fwd_widx;
			if ((uint8_t)(w - fwd_ridx) < sizeof(fwd_cbuf)) {
				fwd_cbuf[FWD_CBUF_IDX(w)] = c;
				BARRIER();
				fwd_widx = w + 1;
			}
		}
		if (!fwd_pending) {
			fwd_pending = 1;
			hal_uart_rx(1);
		}
		break;
	default:
		break;
	}
}
#endif

#else /* !CONSOLE_INPUT */

void
cons_rx()
{
}

void
cons_fwd_rx()
{
}

#endif /* CONSOLE_INPUT */

static const uart_config	uart_cfg = {
	HW_UART_BAUDRATE_115200,
	HW_UART_DATABITS_8,
	HW_UART_PARITY_NONE,
	HW_UART_STOPBITS_1,
	0,
	0,
	1,
	HW_DMA_CHANNEL_1,
	HW_DMA_CHANNEL_0,
};

static inline void uart_enable_rx_int(void)
{
        NVIC_DisableIRQ(UART_IRQn);
        HW_UART_REG_SETF(HW_UART1, IER_DLH, ERBFI_dlh0, true);
        NVIC_EnableIRQ(UART_IRQn);
}

#ifdef CONSOLE_FWD
static inline void uart2_enable_rx_int(void)
{
        NVIC_DisableIRQ(UART2_IRQn);
        HW_UART_REG_SETF(HW_UART2, IER_DLH, ERBFI_dlh0, true);
        NVIC_EnableIRQ(UART2_IRQn);
}
#endif

static void
uart_pin_init()
{
	/* Pull RX up momentarily; otherwise the board hangs if there's
	 * no UART adapter connected to it.  */
	hw_gpio_configure_pin(HW_CONSOLE_UART_RX_PORT,
	    HW_CONSOLE_UART_RX_PIN, HW_GPIO_MODE_OUTPUT, HW_GPIO_FUNC_GPIO, 1);
	hw_gpio_set_pin_function(HW_CONSOLE_UART_TX_PORT,
	    HW_CONSOLE_UART_TX_PIN, HW_GPIO_MODE_OUTPUT, HW_GPIO_FUNC_UART_TX);
#ifdef CONSOLE_INPUT
	hw_gpio_set_pin_function(HW_CONSOLE_UART_RX_PORT,
	    HW_CONSOLE_UART_RX_PIN, HW_GPIO_MODE_INPUT,  HW_GPIO_FUNC_UART_RX);
#endif
#ifdef CONSOLE_FWD
	hw_gpio_configure_pin(HW_FWD_UART_RX_PORT,
	    HW_FWD_UART_RX_PIN, HW_GPIO_MODE_OUTPUT, HW_GPIO_FUNC_GPIO, 1);
	hw_gpio_set_pin_function(HW_FWD_UART_TX_PORT,
	    HW_FWD_UART_TX_PIN, HW_GPIO_MODE_OUTPUT, HW_GPIO_FUNC_UART2_TX);
	hw_gpio_set_pin_function(HW_FWD_UART_RX_PORT,
	    HW_FWD_UART_RX_PIN, HW_GPIO_MODE_INPUT,  HW_GPIO_FUNC_UART2_RX);
#endif
}

void
cons_init()
{
	uart_pin_init();
	hw_uart_init(HW_UART1, &uart_cfg);
#ifdef CONSOLE_FWD
	hw_uart_init(HW_UART2, &uart_cfg);
#endif
}

void
cons_reinit()
{
	uart_pin_init();
	hw_uart_reinit(HW_UART1, &uart_cfg);
#ifdef CONSOLE_INPUT
	hw_uart_set_isr(HW_UART1, uart_isr);
	uart_enable_rx_int();
#endif
#ifdef CONSOLE_FWD
	hw_uart_set_isr(HW_UART2, uart2_isr);
	uart2_enable_rx_int();
#endif
}

#else /* !CONFIG_CUSTOM_PRINT */

void	cons_init() {}
void	cons_reinit() {}
void	cons_rx() {}

#endif /* CONFIG_CUSTOM_PRINT */
