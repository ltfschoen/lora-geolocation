# LoRa-Geolocation

This is the implementation of the work I have done in my Master Thesis "Experimental Evaluation of LPWAN Localization in Urban Settings". Further details of the project can be read from the [thesis](https://gitlab.com/balogul/lora-geolocation/blob/master/doc/thesis-ogulcan-bal.pdf).

## Firmware

The firmware developed for this thesis is under the folder [implementation](https://gitlab.com/balogul/lora-geolocation/tree/master/implementation/bal-lora). The implementation has been done on the [MatchX](https://www.matchx.io/) product [LPWAN DevKit](https://www.matchx.io/product/lpwan-dev-kit/) with DA14680 BLE 4.2 SoC from Dialog Semiconductor.

## Installation

In order to use this code you need the SDK provided by Dialog [here](https://www.dialog-semiconductor.com/products/connectivity/bluetooth-low-energy/smartbond-da14680-and-da14681). Download the latest SDK (DA1468x SDK1.0.14.1081) from there by registering to their website and unzip it to your workspace. It is important to clone this repo under the same folder with the SDK for smooth development as shown below.

```
    .
    ├── DA1468x_DA15xxx_SDK_1.0.14.1081     # SDK folder
    └── bal-lora                            # Firmware folder
```

## Usage

You can use the Eclipse based SmartSnippets IDE for development. Download the latest version from the [website](https://www.dialog-semiconductor.com/products/connectivity/bluetooth-low-energy/smartbond-da14680-and-da14681) under "Development Tools". After installing, choose the SDK folder as your workspace and go to "File->Import->General->Existing Projects into Workspace". Browse and select the firmware folder to find the project, then click finish to import it. You can use different build configurations by Dialog. Please refer to the user manual of SmartSnippets Studio [UM-B-057](https://www.dialog-semiconductor.com/sites/default/files/user_manual_um-b-057_0.pdf) for further details on how to use this IDE.