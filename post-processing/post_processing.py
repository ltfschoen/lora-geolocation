# -*- coding: utf-8 -*-
"""
Python Implementation for the Post-Processing of the data from outdoor experiment.
"""

!pip install utm==0.5.0

from google.colab import files
uploaded = files.upload()

import pandas as pd
pd.read_csv('bal_database_final.csv').head()

import numpy as np
import math
import cmath
from scipy.optimize import fsolve
import utm

import pandas as pd

# Definition of the centroid calculator.
def centroid(ax,ay,bx,by,cx,cy):
	ux = (ax + bx + cx) / 3.0
	uy = (ay + by + cy) / 3.0
	return (ux, uy)
 
# Definition of the circumcenter calculator.
def circumcenter(ax,ay,bx,by,cx,cy):
    d = 2 * (ax * (by - cy) + bx * (cy - ay) + cx * (ay - by))
    ux = ((ax * ax + ay * ay) * (by - cy) + (bx * bx + by * by) * (cy - ay) + (cx * cx + cy * cy) * (ay - by)) / d
    uy = ((ax * ax + ay * ay) * (cx - bx) + (bx * bx + by * by) * (ax - cx) + (cx * cx + cy * cy) * (bx - ax)) / d
    return (ux, uy)

# Definition of the tdoa hyperbola equations.
def ftdoa(z):
    x = z[0]
    y = z[1]
    f = np.zeros(2)
    f[0] = xdif1 * x + ydif1 * y + rdif1 * math.sqrt(x*x+y*y) + 0.5 * ( rdif1*rdif1 - (xdif1*xdif1 + ydif1*ydif1))
    f[1] = xdif2 * x + ydif2 * y + rdif2 * math.sqrt(x*x+y*y) + 0.5 * ( rdif2*rdif2 - (xdif2*xdif2 + ydif2*ydif2))
    
    return f

lat1 = 52.514010
lon1 = 13.335010
lat2 = 52.512820
lon2 = 13.320100
lat3 = 52.505700
lon3 = 13.337800

x1,y1,zone_number,zone_letter = utm.from_latlon(lat1, lon1)
x2,y2,zone_number,zone_letter = utm.from_latlon(lat2, lon2)
x3,y3,zone_number,zone_letter = utm.from_latlon(lat3, lon3)

import io
bal_df = pd.read_csv(io.BytesIO(uploaded['bal_database_final.csv']))

# Preview the first 5 lines of the loaded data 
#print(bal_df.head())

columns = ['xs_fs', 'ys_fs', 'xs_aa', 'ys_aa','err_fs', 'err_aa', 'lats_fs', 'lons_fs', 'lats_aa', 'lons_aa']
calculated = pd.DataFrame(data=np.zeros((0,10)))

my_cols = ['scenario', 'sf', 'data_type', 'anchor','solution', 'error', 'tdoa1', 'tdoa2']
final_data = pd.DataFrame(data=np.zeros((0,8)))

#Take anchor n as (0,0)
xref = x1
yref = y1

# Set True to allow filtering.
filtering = False
# Set True to allow grouping.
grouping = False
group_no = 5
cnt_each_gr = 0

# Set True to print each TP.
detailed = False

#Choose the reference anchor.
for an in range(0,9):

    # Initialize variables.
    my_scenario = "static"
    my_sf = "sf12"
    my_data = "nofilter"
    my_anchor = "an1"

    if an >= 3:
        my_data = "group"
        grouping = True
        group_no = 20

    if an >= 6:
        my_data = "filtered"
        filtering = True

    if an >= 9:
        my_data = "onlyfilter"
        filtering = True
        grouping = False
        group_no = 1
    
    if an % 3 == 0:
        my_anchor = "an1"
        xref = x1
        yref = y1
        xdif1 = x2 - xref
        ydif1 = y2 - yref
        xdif2 = x3 - xref
        ydif2 = y3 - yref
        
    if an % 3 == 1:
        my_anchor = "an2"
        xref = x2
        yref = y2
        xdif1 = x1 - xref
        ydif1 = y1 - yref
        xdif2 = x3 - xref
        ydif2 = y3 - yref
        
    if an % 3 == 2:
        my_anchor = "an3"
        xref = x3
        yref = y3
        xdif1 = x1 - xref
        ydif1 = y1 - yref
        xdif2 = x2 - xref
        ydif2 = y2 - yref
        
    rdif1max = math.sqrt(xdif1*xdif1+ydif1*ydif1) * 0.5
    rdif2max = math.sqrt(xdif2*xdif2+ydif2*ydif2) * 0.5

    # Calculate the centroid or circumcenter of the triangle.
    #xmid,ymid = centroid(x1,y1,x2,y2,x3,y3)
    xmid,ymid = circumcenter(x1,y1,x2,y2,x3,y3)
    xmid-=xref
    ymid-=yref

    xdifmax = 2 * abs(xmid) + 300
    ydifmax = 2 * abs(ymid) + 300

    sum_err_fs = 0
    cnt_err_fs = 0
    sum_err_aa = 0
    cnt_err_aa = 0
    cnt_total = 0

    if grouping:
        group_div = 100 / group_no
    else:
        group_div = 100
        group_no = 1

    cnt_grp_no = 0
    sum_rdif1 = 0
    sum_rdif2 = 0

    #Iterate through the whole file.
    for i in range(len(bal_df)):
        cnt_total += 1

        if i == 0 or i == 1800:
            my_scenario = "static"
        if i == 1600 or i == 3400:
            my_scenario = "walking"

        if i == 0:
            my_sf = "sf12"
        if i == 1800:
            my_sf = "sf7"

        if xref == x1:
            tdoa1 = (bal_df.loc[i,'toa_ns_a2'] - bal_df.loc[i,'toa_ns_a1'])
            tdoa2 = (bal_df.loc[i,'toa_ns_a3'] - bal_df.loc[i,'toa_ns_a1'])
        if xref == x2:
            tdoa1 = (bal_df.loc[i,'toa_ns_a1'] - bal_df.loc[i,'toa_ns_a2'])
            tdoa2 = (bal_df.loc[i,'toa_ns_a3'] - bal_df.loc[i,'toa_ns_a2'])
        if xref == x3:
            tdoa1 = (bal_df.loc[i,'toa_ns_a1'] - bal_df.loc[i,'toa_ns_a3'])
            tdoa2 = (bal_df.loc[i,'toa_ns_a2'] - bal_df.loc[i,'toa_ns_a3'])

        rdif1 = 0.3 * tdoa1
        rdif2 = 0.3 * tdoa2

        lat_g = bal_df.loc[i,'lat_g'] / 600000.0
        lon_g = bal_df.loc[i,'lon_g'] / 600000.0
        
        # Set True to allow filtering.
        if filtering:
        
            # This part assumes sender is in the middle if one receiver didn't get data. 
            if not (math.isnan(rdif1) and math.isnan(rdif2)):
                if math.isnan(rdif1):
                    rdif1 = 0
                if math.isnan(rdif2):
                    rdif2 = 0

            # This part limits the distance difference to half the distance between anchors.
            if rdif1 > rdif1max:
                rdif1 = rdif1max
            if rdif1 < -rdif1max:
                rdif1 = -rdif1max
            if rdif2 > rdif2max:
                rdif2 = rdif2max
            if rdif2 < -rdif2max:
                rdif2 = -rdif2max    
                
        is_na = False
        
        if math.isnan(rdif1):
            is_na = True
            
        if math.isnan(rdif2):
            is_na = True
            
        if grouping:  
            if is_na == False:
                cnt_grp_no += 1
                sum_rdif1 += rdif1
                sum_rdif2 += rdif2

            if cnt_grp_no == group_no:            
                rdif1 = sum_rdif1 / cnt_grp_no
                rdif2 = sum_rdif2 / cnt_grp_no
                sum_rdif1 = 0
                sum_rdif2 = 0
                cnt_grp_no = 0
            else:
                if i % 100 == 99:
                    if cnt_grp_no != 0:
                        rdif1 = sum_rdif1 / cnt_grp_no
                        rdif2 = sum_rdif2 / cnt_grp_no
                    sum_rdif1 = 0
                    sum_rdif2 = 0                
                    cnt_grp_no = 0
                else:        
                    continue
        
        xg,yg,zone_number,zone_letter = utm.from_latlon(lat_g, lon_g)   
        xg -= xref
        yg -= yref

        # Skip the solution if is_na
        if not is_na:
            # This is the solution for TDOA hyperbola solving. 2017
            z, d, ier, msg = fsolve(ftdoa, [xmid,ymid], full_output = True)
            if(ier == 1):
                xs_fs, ys_fs = z[0], z[1]
            else:
                is_na = True

        if is_na or (filtering and ( (abs(xs_fs) > xdifmax) or (abs(ys_fs) > ydifmax) ) ):
            xs_fs = None
            ys_fs = None
            err_fs = None
            lats_fs = None
            lons_fs = None
        else:
            err_fs = math.sqrt((xs_fs - xg) * (xs_fs - xg) + (ys_fs - yg) * (ys_fs - yg))
            lats_fs, lons_fs = utm.to_latlon(xref + xs_fs, yref + ys_fs, zone_number, zone_letter)
            sum_err_fs += err_fs
            cnt_err_fs += 1

        # Skip the solution if is_na
        if not is_na:
            # This is the solution for arbitrary array.
            mx = np.array([[xdif1,ydif1],[xdif2,ydif2]])
            mx = np.linalg.inv(mx)
            k = - mx[0][0] * rdif1 - mx[0][1] * rdif2
            l = 0.5 * (-mx[0][0] *(rdif1*rdif1 -(xdif1*xdif1 +ydif1*ydif1))\
                - mx[0][1] * ( rdif2*rdif2 - (xdif2*xdif2 + ydif2*ydif2) ))
            m = - mx[1][0] * rdif1 - mx[1][1] * rdif2
            n = 0.5 * (-mx[1][0] *(rdif1*rdif1 -(xdif1*xdif1 +ydif1*ydif1))\
                - mx[1][1] * ( rdif2*rdif2 - (xdif2*xdif2 + ydif2*ydif2) ))
            e = k * k + m * m - 1
            f = 2 * (k * l + m * n )
            g = l * l + n * n
            # Calculate the discriminant and find two roots.
            h = (f**2) - (4*e*g)
            sol1 = (-f-cmath.sqrt(h))/(2*e)
            sol2 = (-f+cmath.sqrt(h))/(2*e)
            # Choose the positive root.
            if (sol1 > 0) and (sol1.imag == 0):
                # Check if both roots are positive.
                if (sol2 > 0) and (sol2.imag == 0):
                    xs1, ys1 = k * sol1.real + l, m * sol1.real + n
                    # Check if the location is within limits.
                    if (abs(xs1) < xdifmax) and (abs(ys1) < ydifmax):
                        sol = sol1.real
                    else:
                        xs2, ys2 = k * sol2.real + l, m * sol2.real + n
                        # Check if the location is within limits.
                        if (abs(xs2) < xdifmax) and (abs(ys2) < ydifmax):
                            sol = sol2.real
                        else:
                            sol = 0
                else:
                    sol = sol1.real
            elif (sol2 > 0) and (sol2.imag == 0):
                sol = sol2.real
            else:
                sol = 0
            # Calculate the (xs,ys) pair according to solution.
            if sol > 0:
                xs_aa, ys_aa = k * sol + l, m * sol + n
            else:
                is_na = True

        if is_na or (filtering and ( (abs(xs_aa) > xdifmax) or (abs(ys_aa) > ydifmax) ) ):
            xs_aa = None
            ys_aa = None
            err_aa = None
            lats_aa = None
            lons_aa = None
        else:
            err_aa = math.sqrt((xs_aa - xg) * (xs_aa - xg) + (ys_aa - yg) * (ys_aa - yg))
            lats_aa, lons_aa = utm.to_latlon(xref + xs_aa, yref + ys_aa, zone_number, zone_letter)
            sum_err_aa += err_aa
            cnt_err_aa += 1
            
        if detailed:
            if i % 100 == 99:
                if cnt_err_aa == 0:
                    print("0\t0\t#N/A\t#N/A")
                else:  
                    print(cnt_err_fs/(cnt_total / group_no),"\t",cnt_err_aa /(cnt_total / group_no),"\t",round(sum_err_fs/cnt_err_fs),"\t",round(sum_err_aa/cnt_err_aa))
                sum_err_fs = 0
                cnt_err_fs = 0
                sum_err_aa = 0
                cnt_err_aa = 0
                cnt_total = 0
        else:
            if i == 1599 or i == 1799 or i == 3399 or i == 3599:
                if cnt_err_aa == 0:
                    print("0\t0\t#N/A\t#N/A")
                else:
                    print(cnt_err_fs/(cnt_total / group_no),"\t",cnt_err_aa /(cnt_total / group_no),"\t",round(sum_err_fs/cnt_err_fs),"\t",round(sum_err_aa/cnt_err_aa))
                    sum_err_fs = 0
                    cnt_err_fs = 0
                    sum_err_aa = 0
                    cnt_err_aa = 0
                    cnt_total = 0

        row=pd.Series((xs_fs, ys_fs, xs_aa, ys_aa, err_fs, err_aa, lats_fs, lons_fs, lats_aa, lons_aa))
        calculated = calculated.append(row, ignore_index=True)

        row_fs = pd.Series((my_scenario, my_sf, my_data, my_anchor,"fs", err_fs, tdoa1, tdoa2))
        row_aa = pd.Series((my_scenario, my_sf, my_data, my_anchor,"aa", err_aa, tdoa1, tdoa2))
        final_data = final_data.append(row_fs, ignore_index = True)
        final_data = final_data.append(row_aa, ignore_index = True)

        if grouping:
            cnt_each_gr += 1
            if i % 100 == 99:
                #print("ERROR",cnt_each_gr)
                if cnt_each_gr != group_div:
                    repeat = int(group_div - cnt_each_gr)
                    for rep in range(repeat):
                        row_fs = pd.Series((my_scenario, my_sf, my_data, my_anchor,"fs",None))
                        row_aa = pd.Series((my_scenario, my_sf, my_data, my_anchor,"aa",None))
                        final_data = final_data.append(row_fs, ignore_index = True)
                        final_data = final_data.append(row_aa, ignore_index = True)
                cnt_each_gr = 0

    print("\n")

calculated.columns = columns
print('Anchor',xref,'Done!')

final_data.columns = my_cols
print(final_data.head())

from google.colab import files
calculated.to_csv('bal_results_an1_nofilter.csv', index=True)
files.download('bal_results_an1_nofilter.csv')

from google.colab import files
final_data.to_csv('final_data.csv', index=False)
files.download('final_data.csv')